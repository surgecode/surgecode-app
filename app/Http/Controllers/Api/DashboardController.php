<?php

namespace App\Http\Controllers\Api;

use App\ClientContact;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //

    public function getClients()
    {

        // get total clients
        $data = ClientContact::all();
        // response
        return response()->json(['data' => $data], 200);
    }
}
