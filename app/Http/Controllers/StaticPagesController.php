<?php

namespace App\Http\Controllers;

class StaticPagesController extends Controller
{
    //
    public function index()
    {
        return view('pages.landing');
    }

    public function login()
    {
        return view('pages.login');
    }
}
