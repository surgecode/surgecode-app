<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
    //
    protected $table = 'client_contacts';
    protected $fillable = ['fullname', 'email', 'phone'];
}
