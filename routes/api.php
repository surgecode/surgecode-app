<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// USER LOGIN & REGISTER ROUTES

// CLIENT CONTACT INFO ROUTES
Route::apiResources(['clients' => 'Api\ClientContactController']);

Route::get('getClients', 'Api\DashboardController@getClients');
