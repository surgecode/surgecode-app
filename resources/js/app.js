/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import VueProgressBar from "vue-progressbar";
import VueRouter from "vue-router";
import Swal from "sweetalert2";
window.Swal = Swal;

const options = {
    color: "#673ab7",
    failedColor: "#874b4b",
    thickness: "8px",
    transition: {
        speed: "0.2s",
        opacity: "0.6s",
        termination: 300
    },
    autoRevert: true,
    location: "top",
    inverse: false
};

Vue.use(VueProgressBar, options);
Vue.use(VueRouter);

// Routes
let routes = [
    {
        path: "/dashboard",
        component: require("./components/admin/Dashboard.vue").default
    },
    {
        path: "/clients",
        component: require("./components/admin/ClientsComponent.vue").default
    }
];
const router = new VueRouter({
    routes
});
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component(
    "nav-component",
    require("./components/NavComponent.vue").default
);
Vue.component(
    "bookings-component",
    require("./components/ContactComponent.vue").default
);
Vue.component(
    "header-component",
    require("./components/HeaderComponent.vue").default
);
Vue.component(
    "about-component",
    require("./components/AboutComponent.vue").default
);
Vue.component(
    "features-component",
    require("./components/FeatureComponent.vue").default
);
Vue.component(
    "team-component",
    require("./components/TeamComponent.vue").default
);
Vue.component(
    "connect-component",
    require("./components/ConnectComponent.vue").default
);
Vue.component(
    "footer-component",
    require("./components/FooterComponent.vue").default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    router
});
