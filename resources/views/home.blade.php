@extends('layouts.dashboard')

@section('content')
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white"
        data-image="{{ asset('img/sidebar-1.jpg') }}">
        <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
        <div class="logo">
            <a href="/" class="simple-text logo-normal">
                <img src="{{ asset('img/logo/logo-l.png') }}" alt="" style="width: 15rem;">
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item active">
                    <router-link class="nav-link" to="/dashboard">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </router-link>
                </li>
                <li class="nav-item ">
                    <router-link class="nav-link" to="/profile">
                        <i class="material-icons">whatshot</i>
                        <p>User Profile</p>
                    </router-link>
                </li>
                <li class="nav-item ">
                    <router-link class="nav-link" to="/clients">
                        <i class="material-icons">people</i>
                        <p>My Clients</p>
                    </router-link>
                </li>
                <li class="nav-item ">
                    <router-link class="nav-link" to="/devs">
                        <i class="material-icons">palette</i>
                        <p>Devs/Designers</p>
                    </router-link>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                        <i class="material-icons">toggle_off</i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="#pablo">Dashboard</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <form class="navbar-form">
                        <div class="input-group no-border">
                            <input type="text" value="" class="form-control" placeholder="Search...">
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <router-link class="nav-link" to="/profile">
                                <i class="material-icons">whatshot</i>
                                <p>User Profile</p>
                            </router-link>
                        </li>
                        <li class="nav-item ">
                            <router-link class="nav-link" to="/clients">
                                <i class="material-icons">people</i>
                                <p>My Clients</p>
                            </router-link>
                        </li>
                        <li class="nav-item ">
                            <router-link class="nav-link" to="/devs">
                                <i class="material-icons">palette</i>
                                <p>Devs/Designers</p>
                            </router-link>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                                          document.getElementById('logout-form').submit();">
                                <i class="material-icons">toggle_off</i>
                                <p>
                                    {{ __('Logout') }}
                                </p>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <router-view></router-view>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="/">
                                Home
                            </a>
                        </li>

                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    , by
                    <a href="/" target="_blank">SurgeCode Web Solutions</a> for a better web.
                </div>
            </div>
        </footer>
    </div>
</div>
@endsection
