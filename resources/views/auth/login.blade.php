@extends('layouts.dashboard')
@section('custom_css')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 centerForm card-padding">
            <div class="card">
                <div class="card-header card-header-primary card-header__bg">
                    <img class="center-img" src="/img/logo/logo-75.png" alt>
                    <h4 class="card-title center-align">Login</h4>
                    <p class="card-category center-align font-16">For Company Developers Only.</p>
                </div>

                <div class="card-body card-body-padding">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group bmd-form-group mt-3">
                                    <label for="email" class="bmd-label-floating">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="material-icons form-control-feedback">clear</span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group bmd-form-group mt-3">
                                    <label for="password" class="bmd-label-floating">{{ __('Password') }}</label>
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="current-password">

                                    @error('password')
                                    <span class="material-icons form-control-feedback">clear</span>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btnn btnn--gradient btnn--animated">
                            {{ __('Login') }}
                        </button>
                        <a href="/" style="margin-left: 2rem;">Go Back &lArr;</a>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
