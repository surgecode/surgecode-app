@extends('layouts.app')
@section('content')
<nav-component></nav-component>
<vue-progress-bar></vue-progress-bar>
<header-component></header-component>
<!-- body content after th header area -->
<main>
<about-component></about-component>
<features-component></features-component>
<team-component></team-component>
<connect-component></connect-component>
<!-- contact form components -->
<section class="section-bookings" id="contact">
<div class="row">
    <div class="book">
        <div class="book__bookings-form">
            <bookings-component></bookings-component>
        </div>
    </div>
</div>
</section>
<footer-component></footer-component>
</main>
@endsection