$(document).ready(() => {

$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
     $("#nav-toggle").prop("checked", false);
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos}, 2500);
});

$('#openModal').on('click', function() {
    $('.popup').addClass('popup__active');
})
$('#openModal2').on('click', function() {
    $('.popup').addClass('popup__active');
})

$('.popup__close').on('click', function() {
    $('.popup').removeClass('popup__active');
})
});
